<?php

class Pet extends Eloquent
{

    protected $table = "pets";
    protected $guarded = array('id');

    public function images()
    {
        return $this->hasMany('Image', 'pet_id');
    }

    public function seller()
    {
        return $this->BelongsTo('User', 'user_id');
    }

    public function short_description()
    {
        if (strlen($this->description) > 100)
            return substr($this->description, 0, 100) . "...";
        else
            return $this->description;
    }
}