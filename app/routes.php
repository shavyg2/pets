<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//homepage

Route::get('/', array('as' => 'homepage', 'uses' => 'HomeController@main'));

//submit pet
/*
allow for pictures
number of puppies
species
breed when they will become available
payment options
different  methods/prices options to announce their puppies
multiple ways to sort pets
*/

route::get("pet/new", array('as' => 'new_pet', 'uses' => 'HomeController@new_pet'));
route::post("pet/new", array('as' => 'save_pet', 'uses' => 'HomeController@save_pet'));


//allow for pet buyer to see extreme animals
/*
allow for special purchases
you can sign up for a mailing list
*/
route::get('pet/browse', array('as' => 'pet_browse', 'uses' => 'HomeController@pet_browse'));
route::get('pet/browse/{id}', array('as' => 'pet_detail', 'uses' => 'HomeController@pet_detail'));





route::get('register',array('as'=>'auth','uses'=>'HomeController@login'));
route::post('signin',array('as'=>'signin','uses'=>'HomeController@signin'));
route::post('create/user',array('as'=>'create_user','uses'=>'HomeController@create_user'));
//logout
route::get('logout',array('as'=>'logout','uses'=>'HomeController@logout'));



route::get('icon',function(){
    $icon=new Iconpack();
    $icon->all_icons_name();
});


//Search
route::get('search',array('as'=>'search','uses'=>'HomeController@search'));


//AutoComplete
route::get('breeds',array('uses'=>'HomeController@auto_breed','as'=>'all_breed'));
route::get('names',array('uses'=>'HomeController@auto_name','as'=>'all_name'));
route::get('colors',array('uses'=>'HomeController@auto_color','as'=>'all_color'));



