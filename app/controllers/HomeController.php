<?php


class HomeController extends BaseController
{


    public function __construct(User $user, Pet $pet)
    {
        $this->user = $user;
        $this->pet = $pet;
    }

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function main()
    {
        $user = null;
        if (Session::has('id')) {
            $user = $this->user->find(Session::get('id'));
        }
        $pet = $this->pet->orderBy('created_at', 'desc')->take(5)->get();

        $your_posts = null;
        if (Session::has('id'))
            $your_posts = $this->pet->where('user_id', Session::get('id'))->orderBy('created_at', 'desc')->paginate(8);

        return View::make("homepage")->with('user', $user)
            ->with('recent_pets', $pet)
            ->with('your_posts', $your_posts);
    }

    public function new_pet()
    {
        $user = null;
        if (Session::has('id')) {
            $user = $this->user->find(Session::get('id'));
        }

        return View::make("new_pet")->with('user', $user)
            ->with('breeds', $this->auto_breed())
            ->with('colors', $this->auto_color())
            ->with('names', $this->auto_name());
    }


    public function login()
    {
        return View::make("login")
            ->with('login_detail', true);
    }

    public function signin()
    {
        $last = $this->user->where(function ($query) {
            $user = Input::except('_token');
            $query->where('username', $user['username']);
            $query->where('password', $user['password']);
        })->first();

        if ($last != null) {
            Session::put('id', $last->id);
            return Redirect::route('homepage');
        } else {
            return Redirect::route('auth')->withInput();
        }
    }

    public function create_user()
    {
        $user = Input::except('_token');
        $lastUser = $this->user->create($user);

        $last = $this->user->where(function ($query) {
            $user = Input::except('_token');
            $query->where('username', $user['username']);
            $query->where('password', $user['password']);
        })->first();

        Session::put('id', $last->id);
        return Redirect::route('homepage');

    }

    public function logout()
    {
        Session::forget('id');
        return Redirect::route('homepage');
    }

    public function pet_browse()
    {
        $user = null;
        if (Session::has('id')) {
            $user = $this->user->find(Session::get('id'));
        }

        $pet = $this->pet->orderBy('created_at', 'desc')->paginate(5);

        return View::make("browse_pet")
            ->with('user', $user)
            ->with('pets', $pet);
    }

    public function search()
    {
        $user = null;
        if (Session::has('id')) {
            $user = $this->user->find(Session::get('id'));
        }

        $pet = $this->pet->where(function ($query) {
            $search = Input::get('search');
            $search = explode(' ', $search);

            foreach ($search as $q) {
                $query->orWhere('name', "LIKE", "%" . $q . "%");
                $query->orWhere('breed', "LIKE", "%" . $q . "%");
                $query->orWhere('description', "LIKE", "%" . $q . "%");
            }
        })->get();

        $this->ranking($pet);

        $pet = $this->quick_sort($pet);

        dd();

        return View::make("search")
            ->with('user', $user)
            ->with('pets', $pet);
    }

    public function ranking($objects)
    {
        foreach ($objects as $object) {
            $object->rank = 0;
        }

        $search = Input::get('search');
        $search = explode(' ', $search);
        foreach ($objects as $object) {

            foreach ($search as $q) {
                if ($q == "")
                    continue;
                $object->rank += 20 * substr_count(strtolower($object->name), strtolower($q));
                $object->rank += 5 * substr_count(strtolower($object->breed), strtolower($q));
                $object->rank += substr_count(strtolower($object->description), strtolower($q));
            }
        }
    }

    public function quick_sort($objects)
    {
        if (count($objects) <= 1) {
            return $objects;
        } else {
            $size = count($objects);
            $count = 0;
            $left = array();
            $right = array();

            do {
                $left[] = $objects[$count++];
            } while ($count < $size / 2);

            while ($count < $size) {
                $right[] = $objects[$count++];
            }


            $left = $this->quick_sort($left);
            $right = $this->quick_sort($right);

            $objects = array();

            $left_index = 0;
            $right_index = 0;

            while ($left_index < count($left) && $right_index < count($right)) {
                if ($left[$left_index]->rank > $right[$right_index]->rank) {
                    $objects[] = $left[$left_index++];
                } else {
                    $objects[] = $right[$right_index++];
                }
            }


            while ($left_index < count($left)) {
                $objects[] = $left[$left_index++];
            }

            while ($right_index < count($right)) {
                $objects[] = $right[$right_index++];

            }

            return $objects;
        }
    }

    public function pet_detail($id)
    {
        $user = null;
        if (Session::has('id')) {
            $user = $this->user->find(Session::get('id'));
        }

        $pet = $this->pet->find($id);

        return View::make('pet_detail')
            ->with('pet', $pet)
            ->with('user', $user);
    }

    public function save_pet()
    {
        $pet = Input::only('title', 'breed', 'years', 'months', 'color', 'feet', 'inches', 'description', 'favourite_activity', 'quantity', 'price');
        $pet['user_id'] = Session::get('id');
        $bad = false;


        if (empty($pet['title'])) {
//            $pet['name'] = 'Not Specified';
            $bad = true;
        }
        if (empty($pet['breed'])) {
            //$pet['breed'] = 'Not Specified';
            $bad = true;
        }
        if (empty($pet['years'])) {
            $pet['years'] = '0';
            $bad = true;
        }
        if (empty($pet['months'])) {
            $pet['months'] = '0';
            $bad = true;
        }
        if (empty($pet['quantity'])) {
            $pet['quantity'] = '1';
            $bad = true;
        }
        if (empty($pet['color'])) {
            $pet['color'] = 'Not Specified';
        }
        if (empty($pet['feet']) && empty($pet['inches'])) {
            $bad = true;
        }
        if (empty($pet['description'])) {
//            $pet['description'] = '';
            $bad = true;
        }
        if (empty($pet['favourite_activity'])) {
            $pet['favourite_activity'] = 'Not Specified';
        }

        if (true) {
            $this->pet->create($pet);
            $pet = DB::table('pets')->get();
            $pet = end($pet);
            $this->loadImages($pet->id);
            return Redirect::route('homepage');
        }

    }

    private function loadImages($id)
    {
        $files = Input::file('images');
        $path = public_path('upload_files/');
        foreach ($files as $file) {
            $file_extension = $file->getClientOriginalExtension();
            do {
                $filename = uniqid(md5(uniqid())) . "." . $file_extension;
            } while (file_exists($path . $filename));

            $image = new Image;
            $image->pet_id = $id;
            $image->path = 'upload_files/' . $filename;
            $image->name = $file->getClientOriginalName();
            $image->save();

            $file->move('upload_files/', $filename);
        }
    }

    public function auto_breed()
    {
        $results = array();
        foreach (Pet::all() as $pet) {
            $results[] = ucwords($pet->breed);
        }
        return array_unique($results);
    }

    public function auto_name()
    {
        $breeds = array();
        foreach (Pet::all() as $pet) {
            $breeds[] = ucwords($pet->name);
        }
        return array_unique($breeds);
    }

    public function auto_color()
    {
        $breeds = array();
        foreach (Pet::all() as $pet) {
            $breeds[] = ucwords($pet->color);
        }
        return array_unique($breeds);
    }
}