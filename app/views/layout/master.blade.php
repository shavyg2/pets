<!doctype html>
<html lang="en-US">
	@include('partials._header')
<body>
	@include('partials.menu')
	@yield('content')
	@include('partials._footer')
</body>
</html>