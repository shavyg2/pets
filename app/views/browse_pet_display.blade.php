<div class="result row">
    <div class="large-5 column">
        <div class="middle-align img">
            <a href="{{URL::route('pet_detail',array('id'=>$pet->id))}}">
                {{HTML::image($pet->images->first()->path,null,array('class'=>'img-responsive','style'=>'margin:auto;'))}}
            </a>
        </div>
    </div>
	<div class="info-section large-15 column">
		<h1>
            <a href="{{URL::route('pet_detail',array('id'=>$pet->id))}}">
                {{ucwords($pet->title)}}
            </a></h3>
        </h1>
		<p class="type">Type: {{ucwords($pet->breed)}} </p>
		<p class="description">{{ucwords($pet->short_description())}} </p>
	</div>
	<div class="large-4 column">
		<p>Price: ${{$pet->price}}</p>
	</div>
	
</div>
