@extends('layout.master')

@section('content')
<?php $icon = new Iconpack; ?>
<section class="content">
    <div class="body row">
        <br>

        <h1>{{$pet->title}}</h1>

        <div class="row">
            <div class="columns large-12">


                <div class="row">
                    <div class="columns large-20">
                        {{HTML::image($pet->images->first()->path,null,array('id'=>'img-display-swap','class'=>'img-responsive','style'=>'margin:auto;'))}}
                    </div>
                </div>
                <br>

                <div class="row">

                    <?php $count = 1; ?>
                    @foreach($pet->images->all() as $image)

                    <div class="columns large-6">
                        <div class="img transfer">
                            {{HTML::image($image->path,null,array('class'=>'img-responsive'))}}
                        </div>
                    </div>
                    <div class="columns large-1 no-show">this is my typingh some shit</div>
                    @if($count>0 && $count%3==0)
                    <div class="columns large-3 no-show"><p>Aren't you glad this is clean and a no volger!</p></div>
                    @endif
                    <?php $count++; ?>
                    @endforeach
                </div>

            </div>
            <div class="columns large-12">

                <div class="row"><h6 class="inline">Animal</h6> {{$pet->breed}}</div>
                <div class="row"><h6 class="inline">Age</h6> {{$pet->years}}yr(s) {{$pet->months}}mth(s)</div>
                <div class="row"><h6 class="inline">Height</h6> {{$pet->feet}}ft {{$pet->inches}}in</div>
                <div class="row"><h6 class="inline">Color</h6> {{$pet->color}}</div>
                <div class="row"><h6 class="inline">Description</h6></div>
                <div class="col-md-8">{{$pet->description}}</div>
                <br>

                <div class="row">
                    <h4 class="text-danger">Favourite Activity</h4> {{$pet->favourite_activity}}
                </div>

                <br>

                <div class="row">
                    <h4 class="text-danger">Seller</h4>
                    {{ucwords($pet->seller->fname." ". $pet->seller->lname)}}
                </div>
                <div class="row">
                    {{$icon->email(40,40,'#C72E2E')}} {{$icon->facebook_square(40,40,'#3b5998')}}
                </div>
            </div>
        </div>


    </div>
</section>

@stop