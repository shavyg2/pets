<section class="header">
    <div class="row">
        <div class="large-6 column">
            <a class="logo" href="/"><img src="/Petorama/img/logo.png"/></a>
        </div>
        <div class="large-14 column">
            <ul class="inline-list right">
                <li><a href="{{URL::route('homepage')}}">Home</a></li>
                <li><a href="{{URL::route('pet_browse')}}">Browse Pets</a></li>
                <li><a href="{{URL::route('homepage')}}">Forum</a></li>
                <li><a href="{{URL::route('homepage')}}">Contact Us</a></li>
            </ul>
        </div>


        <div class="large-4 column">
            <ul class="right">
                <li class="accessed">
                    <ul class="inline-display">
                        @if(!isset($user))
                        <li>
                            <a href="#" data-dropdown="loginForm" class="access left">Login</a>
                        </li>
                        <li>
                            <a href="{{URL::route('auth')}}" class="access right">Register</a>
                        </li>
                        @else
                        <li><a href="{{URL::route('logout')}}" class="right">Logout</a></li>
                        @endif
                    </ul>
                </li>
                <li class=" roomy" ><a href="{{URL::route('new_pet')}}" class="postade custom-button">Post Free Ad</a></li>
            </ul>
        </div>

        <!-- Login Drop-Down Form -->
        <div id="loginForm" class="f-dropdown content small" data-dropdown-content>
            {{Form::open(array('url'=>URL::route('signin'),'method'=>'post', 'id'=>'login'))}}
            <i class="icon-user"></i>{{Form::text('username',Input::old('username'),array('placeholder'=>'Username'))}}
            <i class="icon-lock"></i>{{Form::password('password',array('placeholder'=>'Password'))}}
            <p>Don't have an account?</p>
            {{Form::close()}}
            <a class="register" href="{{URL::route('auth')}}">Register Now!</a>
            <a id="loginButton" class="login-button" href="#"><i class="icon-chevron-sign-right"></i></a>
        </div>
        <script>
            $("#loginButton").on('click', function () {
                $("#login").submit();
            });
        </script>
    </div>
</section>



