<section class="footer">
	<div class="row">
		<div class="large-12 column">
			<ul class="inline-list">
				<li><a href="">Home</a></li>
				<li><a href="">Privacy Policy</a></li>
				<li><a href="">Site Map</a></li>
			</ul>
		</div>
		<div class="large-12 column">
			<p class="right">Copyright 2013 - PetGalore </p>
		</div>
	</div>
</section>