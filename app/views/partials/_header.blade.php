<head>
    <meta charset="UTF-8">
    @if(isset($title))
    <title>{{$title}}</title>
    @endif

    <!--    jquery-->
	<script>
	  document.write('<script src=/js/vendor/'
		+ ('__proto__' in {} ? 'zepto' : 'jquery')
		+ '.js><\/script>');
	</script>
	<script src="/js/foundation/foundation.js"></script>
	<script src="/js/foundation/foundation.dropdown.js"></script>
	<script src="/js/foundation/foundation.reveal.js"></script>
	<script>
		$(document).foundation();
	</script>

<!--css-->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    {{HTML::style('css/normalize.css')}}
    {{HTML::style('css/foundation.min.css')}}
    {{HTML::style('css/style.css')}}
    {{HTML::style('css/quickfix.css')}}

<!--    hos fix js-->
    {{HTML::script('js/hotfix.js')}}

</head>