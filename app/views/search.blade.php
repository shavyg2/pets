@extends('layout.master')

@section('content')
<section class="content">
    <div class="body row">
        <h1> Search Pets</h1>

        <div class="sort large-6 columns">
            <h5> Refine your search </h5>

            <form>
                <label> Pet Type </label>
                <select>
                    <option selected>Pet Type(Any)</option>
                    <option>Cats</option>
                    <option>Birds</option>
                    <option>Reptiles</option>
                    <option>Chickens</option>
                </select>
                <label> Pet Breed </label>
                <select>
                    <option selected>Pet Breed(Any)</option>
                    <option>Cats</option>
                    <option>Birds</option>
                    <option>Reptiles</option>
                    <option>Chickens</option>
                </select>
                <label>Results Per Page</label>
                <select>
                    <option>10</option>
                    <option>20</option>
                    <option>30</option>
                    <option>40</option>
                    <option>50</option>
                </select>
                <label>Location</label>
                <input name="location" placeholder="City or Postal Code"/>
                <label>Keywords</label>
                <input name="location" placeholder="Enter Keywords"/>
            </form>
            <a href="" class="button">Submit</a>
        </div>
        <div class="results-box large-17 column">
            <div class="row heading">
                <div class="large-8 column">
                    <div class="large-8 column">
                        <span>Sort By : </span>
                    </div>
                    <div class="large-16 column">
                        <select>
                            <option>Date Added</option>
                            <option>Date Added (Oldest)</option>
                            <option>Price(Lowest)</option>
                            <option>Price(Highest)</
                            </option>
                        </select>
                    </div>
                </div>
                <div class="large-6 column">
                </div>
            </div>
            @foreach($pets as $pet)
            @include('browse_pet_display')
            @endforeach
        </div>

    </div>

</section>
@stop