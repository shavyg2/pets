@extends('layout.master')

@section('content')
<section class="content">
    <div class="body row">
        <h1>Post a Pet Ad</h1>

        <div class="large-24 column">
            {{ Form::open(array('url'=>URL::route('save_pet'),'method'=>'post','enctype'=>'multipart/form-data')) }}


            <!--name-->
            <div class="row">
                <div class="columns large-24">
                    <h5>Post Title</h5>
                </div>
            </div>

            <div class="row">
                <div class="columns large-15">
                    {{ Form::text('title',Input::old('title'),array('class'=>'','placeholder'=>'Title','id'=>'title'))
                    }}
                </div>
            </div>


            <!--breed-->
            <h5>Animal</h5>

            <div class="row">
                <div class="columns large-15">
                    {{
                    Form::text('breed',Input::old('breed'),
                    array('placeholder'=>'Dog, Cats,Dragons...','id'=>'breed'))
                    }}
                </div>

            </div>


            <div class="row">

                <div class="columns large-2">
                    {{ Form::text('years',Input::old('years'), array('class'=>'','placeholder'=>'years')) }}
                </div>
                <div class="columns large-2 push-1">
                    {{ Form::text('months',Input::old('months'), array('class'=>'','placeholder'=>'months')) }}
                </div>

                <div class="columns large-2 push-2">
                    {{
                    Form::text('quantity',Input::old('quantity'),
                    array('class'=>'','placeholder'=>'Quantity'))
                    }}
                </div>

                <div class="columns large-4 push-3">
                    {{ Form::text('price',Input::old('price'), array('placeholder'=>'$ Price')) }}
                </div>
                <div></div>
            </div>


            <br>

            <div class="row">
                <div class="columns large-15">
                    {{ Form::text('color',Input::old('color'),
                    array('id'=>'color_picker','placeholder'=>'Color eg.Black and White')) }}
                </div>

            </div>


            <!--favourite activity-->
            <br>

            <div class="row">
                <div class="columns large-10">
                    {{ Form::text('favourite_activity',Input::old('favourite_activity'),
                    array('class'=>'form-control','placeholder'=>'Favourite Activity')) }}
                </div>
            </div>


            <!--height-->
            <h5>Height</h5>

            <div class="row">
                <div class="columns large-2">
                    {{ Form::text('feet',Input::old('feet'), array('class'=>'form-control','placeholder'=>'Ft')) }}
                </div>

                <div class="columns large-2 push-1">
                    {{ Form::text('inches',Input::old('inches'),array('class'=>'form-control','placeholder'=>'Inch')) }}
                </div>

                <div></div>

            </div>


            <!--description box-->
            <h5>Description</h5>

            <div class="row">
                <div class="columns large-15">
                    {{ Form::textarea('description',Input::old('description'),
                    array('class'=>'text-box','placeholder'=>'',"row"=>"5"))
                    }}
                </div>


            </div>


            <!--image labels-->
            <div class="row">

                {{ Form::label('files','Images') }}
                {{ Form::file('images[]',array('multiple'=>'')) }}
            </div>


            <!--submit button-->
            <div class="row">
                {{ Form::Submit('Save',array('class'=>'button')) }}
                {{Form::close()}}
            </div>


        </div>

    </div>
</section>
@stop