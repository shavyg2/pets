@extends('layout.master')

@section('content')
<section class="content">
    <div class="body row">
        <div>
            <h1>Not A Member
                <small>Sign Up Now</small>
            </h1>
            <hr>

            <h5>User Account</h5>

            <div class="row">
                {{Form::open(array('url'=>URL::route('create_user'),'method'=>'post','role'=>'form','class'=>''))}}
                <!--                    User Info -->


                <div class="columns large-11">
                    {{ Form::label('username','Username',array('class'=>'sr-only')) }}
                    {{ Form::text('username',Input::old('username'),array('placeholder'=>'Username')) }}
                </div>


                <div class="columns large-11">
                    {{ Form::label('password','Password',array('class'=>'sr-only')) }}
                    {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}
                </div>
            </div>

            <!--                     Personal-->

            <div class="row">
                <div class="columns large-24">
                    <h5>User Info</h5>
                </div>
            </div>

            <div class="row">
                <div class="columns large-8">
                    {{Form::label('email','Email',array('class'=>'sr-only'))}}
                    {{
                    Form::text('Email',Input::old('email'),
                    array('class'=>'form-control','placeholder'=>'Email Address'))
                    }}
                </div>
            </div>

            <div class="row">
                <div class="columns large-11">
                    <!--                    firstname and last name-->
                    {{Form::label('fname','First Name',array('class'=>'sr-only'))}}
                    {{
                    Form::text('fname',Input::old('fname'),
                    array('class'=>'form-control','placeholder'=>'First Name'))
                    }}
                </div>


                <div class="columns large-11">
                    {{Form::label('lname','Last Name',array('class'=>'sr-only'))}}
                    {{
                    Form::text('lname',Input::old('lname'),
                    array('class'=>'form-control','placeholder'=>'Last Name'))
                    }}

                </div>
            </div>


            <div class="row">
                <!--contact info-->
                <div class="columns large-24">
                    <h5>Contact Info</h5>
                </div>
            </div>

            <div class="row">

                <div class="columns large-10">
                    {{Form::label('phone','Phone Number',array('class'=>'sr-only'))}}
                    {{
                    Form::input('tel','phone',Input::old('phone'),
                    array('class'=>'form-control','placeholder'=>'Phone Number'))
                    }}
                </div>
            </div>

            <div class="row">
                <div class="columns large-4">
                    {{Form::label('address_num','Street Number',array('class'=>'sr-only'))}}
                    {{

                    Form::text('address_num',Input::old('address_num'),
                    array('class'=>'form-control','placeholder'=>'123'))
                    }}
                </div>

                <div class="columns large-12 push-1">
                    {{Form::label('street_address','Street Name',array('class'=>'sr-only'))}}
                    {{
                    Form::text('street_address',Input::old('street_address'),
                    array('class'=>'form-control','placeholder'=>'My Street'))
                    }}
                </div>

                <div class="columns large-6">
                    {{Form::label('city','City',array('class'=>'sr-only'))}}
                    {{
                    Form::text('city',Input::old('city'),
                    array('class'=>'form-control','placeholder'=>'City'))
                    }}
                </div>
            </div>


            <div class="row">
                <div class="columns large-8">
                    {{Form::label('state','State/Province',array('class'=>'sr-only'))}}
                    {{
                    Form::text('state',Input::old('state'),
                    array('class'=>'form-control','placeholder'=>'State'))
                    }}
                </div>

                <div class="columns large-6">
                    {{Form::label('country','Country',array('class'=>'sr-only'))}}
                    {{
                    Form::text('country',Input::old('country'),
                    array('class'=>'form-control','placeholder'=>'Country'))
                    }}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="columns large-4 right">
                    {{Form::submit('Submit',array('class'=>'button'))}}
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</section>
@stop