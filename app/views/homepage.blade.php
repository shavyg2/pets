@extends('layout.master')


@section('content')


<section class="content">
    <div class="banner row">
        <div class="left large-12 column">
            <img src="Petorama/img/heading.png" alt="heading"/>
            <p>
                With thousands of pet ads posted daily, you'll definitely find the one right for you. Search through our our ads of fabolous furry and not so furry critters!
            </p>
            <div class="form">
                <div class="row">
                    <div class="large-11 column">
                        <label for="type">Pet Type</label>
                        <input id="type" name="type" type="text"/>
                    </div>
                    <div class="large-11 column">
                        <label for="type">Location</label>
                        <input id="type" name="type" type="text"/>
                    </div>
                </div>
                <div class="row">
                    <div class="large-11 column">
                        <label for="type">Search Keywords:</label>
                        {{Form::open(array('url'=>URL::route('search'),'method'=>'get'))}}
                        {{Form::text('search',Input::old('search'))}}
                        {{Form::close()}}
                    </div>
                    <div class="large-11 column">
                        <a href="search.html" class="search button small">Search</a>
                    </div>
                </div>
            </div>
        </diV>
        <div class="right large-12 pull-1 column">
            <img src="Petorama/img/pets.png" alt="heading"/>
        </div>
    </div>
    <div class="recent-nav row">
        <div class="large-24 column">
            <ul class="inline-list">
                <li class="selected"><a href="#">Recently Posted</a></li>
                <li><a href="#">Popular Pets </a></li>
                <li><a href="#">Exotic Pets </a></li>
            </ul>
        </div>
    </div>
    <div class="recent row">
        <div class="large-24 column">
            <ul class="large-block-grid-5">
                @foreach($recent_pets as $pet)
                <li>
                    <div>
                        <div class="middle-align">
                            <div class="img">
                                {{ HTML::image($pet->images()->first()->path) }}
                            </div>
                            <p class="pet-title-even">{{$pet->title}}</p>

                            <p><span class="white right">${{$pet->price}}</span></p>
                        </div>
                    </div>
                </li>
                @endforeach

            </ul>
        </div>
    </div>
</section>

@stop