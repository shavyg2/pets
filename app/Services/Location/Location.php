<?php

namespace Services;

class Location
{

    public static function get_location()
    {
        $ip = Request::getClientIp();
        if (!$ip == "127.0.0.1")
            return file_get_contents("http://ipinfo.io/$ip/json");
        else
            return file_get_contents("http://ipinfo.io/json");
    }

    private static function parse($json)
    {
        $object = json_decode($json);
        if (isset($object->loc)) {
            $location = explode(',', $object->loc);
            $coords = array(
                'latitude' => $location[0],
                'longitude' => $location[1]
            );
            $object->loc=$coords;
        }
        return $object;
    }

    public static function detail()
    {
        $location= self::parse(self::get_location());
        var_dump($location);
    }
}