$(document).ready(function () {
    $(".img").each(function () {
        var image_container = $(this);
        var image = $(image_container.find("img")[0]);
        path = image.attr("src");
        image.css("display","none");
        image_container.css("background-image", "url(" + path + ")");
    });
});